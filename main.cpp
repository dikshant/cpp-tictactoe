#include <iostream>
#include "GameBoard.h"

//Dikshant Adhikari and Rhadhey Patel

using namespace std;


int main() {

    //make an instance
    GameBoard board;

    //run the game
    board.playGame();
    return 0;

};

