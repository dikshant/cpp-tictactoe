// Dikshant Adhikari and Rhadhey Patel

#ifndef TICTACTOE_GAMEBOARD_H
#define TICTACTOE_GAMEBOARD_H

#include "GameBoard.h"
#include <iostream>

using namespace std;

class GameBoard {


public:
    //a variable to store the board
    int board[9] = {9,9,9,9,9,9,9,9,9};

    //a variable to store move
    int move;

public:

    //draws the board
    void drawBoard();

    //gets input from player one
    int getInputOne();

    //checks win for the human player
    int checkWinnerHuman();

    //checks win for the AI overlord
    int checkWinnerComputer();

    //searches the max score for a given node
    int maxSearch(int * hiddenBoard);

    //searches the min score for a given node
    int minSearch(int * hiddenBoard);

    //finds the best move to be made
    int minMax(int * hiddenBoard);

    //runs the game
    void playGame();

    //makes the computer move
    void makeComputerMove();
};

#endif