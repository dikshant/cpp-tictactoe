//
// Dikshant Adhikari and Rhadhey Patel
//

#include "GameBoard.h"

using namespace std;

//Take input for player 1, and places X on the assigned square and draws the new board in pretty form
//filters out unnecessary output
int GameBoard::getInputOne() {
    cout << "Player O, please enter a number 0-8: ";

    int coord;
    cin >> coord;
    if (coord == 0 && board[0] == 9 && board[1] != 0) {
        board[0] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 1 && board[1] == 9 && board[1] != 0) {
        board[1] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 2 && board[2] == 9 && board[2] != 0) {
        board[2] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 3 && board[3] == 9 && board[3] != 0) {
        board[3] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 4 && board[4] == 9 && board[4] != 0) {
        board[4] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 5 && board[5] == 9 && board[5] != 0) {
        board[5] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 6 && board[6] == 9 && board[6] != 0) {
        board[6] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 7 && board[7] == 9 && board[7] != 0) {
        board[7] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else if (coord == 8 && board[8] == 9 && board[8] != 0) {
        board[8] = 1;
        cout<<"The board after your move: "<<endl;
        drawBoard();

    } else {
        getInputOne();
    }
    return coord;
}


//takes the board and checks if the human player won
int GameBoard::checkWinnerHuman() {

    //if 1 wins return 1
    if (board[0] == 1 && board[1] == 1 && board[2] == 1 ||
        board[3] == 1 && board[4] == 1 && board[5] == 1 ||
        board[6] == 1 && board[7] == 1 && board[8] == 1 ||
        board[0] == 1 && board[3] == 1 && board[6] == 1 ||
        board[1] == 1 && board[4] == 1 && board[7] == 1 ||
        board[2] == 1 && board[5] == 1 && board[8] == 1 ||
        board[0] == 1 && board[4] == 1 && board[8] == 1 ||
        board[2] == 1 && board[4] == 1 && board[6] == 1
            ) {
        return 1;
    } else {
        return 0;
    }
}

//checks the board to see if the computer player won
int GameBoard::checkWinnerComputer() {
    if (board[0] == 0 && board[1] == 0 && board[2] == 0 ||
        board[3] == 0 && board[4] == 0 && board[5] == 0 ||
        board[6] == 0 && board[7] == 0 && board[8] == 0 ||
        board[0] == 0 && board[3] == 0 && board[6] == 0 ||
        board[1] == 0 && board[4] == 0 && board[7] == 0 ||
        board[2] == 0 && board[5] == 0 && board[8] == 0 ||
        board[0] == 0 && board[4] == 0 && board[8] == 0 ||
        board[2] == 0 && board[4] == 0 && board[6] == 0
            ) {
        return 1;
    }
    else {
        return 0;
    }

};

//searches finds the max score recursively
int GameBoard::maxSearch(int *hiddenBoard) {
    if (checkWinnerHuman() == 1) {
        return 10;
    }
    else if (checkWinnerComputer() == 1) {
        return -10;
    }
    // simulate all possible moves and find max score
    int best = -9999;
    for (int i = 0; i < 9; i++) {
        if (hiddenBoard[i] == 9) {
            hiddenBoard[i] = 1;
            int nodeScore = minSearch(hiddenBoard);
            if (nodeScore > best) {
                best = nodeScore;
                move = i;
            }
            hiddenBoard[i] = 9;
        }
    }
    return best;
}

//finds the min score recursively
int GameBoard::minSearch(int *hiddenBoard) {

    if (checkWinnerHuman() == 1) {
        return 10;
    }
    else if (checkWinnerComputer() == 1) {
        return -10;
    }

    // simulate all possible moves and finds min score
    int best = 9999;
    for (int i = 0; i < 9; i++) {
        if (hiddenBoard[i] == 9) {
            hiddenBoard[i] = 0;
            int nodeScore = maxSearch(hiddenBoard);
            if (nodeScore < best) {
                best = nodeScore;
                move = i;
            }
            hiddenBoard[i] = 9;
        }
    }
    return best;
}

//recursives calculates the best move to be made and finds the best score
int GameBoard::minMax(int *hiddenBoard) {

    // arbitary number to calculate moves
    int best = 9999;

    // simulate all possible moves
    for (int i = 0; i < 9; i++) {
        if (board[i] == 9) {
            board[i] = 0;
            int nodeScore = maxSearch(hiddenBoard);
            if (nodeScore <= best) {
                best = nodeScore;
                move = i;
            }
            board[i] = 9;
        }
    }
    return move;
}

//makes the computers move on the board
void GameBoard::makeComputerMove() {
    int b = minMax(board);
    board[b] = 0;
    drawBoard();
}

//draws the pretty board
void GameBoard::drawBoard() {

    //makes the board look not ugly
    string val[9];
    for (int i = 0; i < 9; i++) {
        if (board[i] == 0) {
            val[i] = 'X';
        } else if (board[i] == 1) {
            val[i] = 'O';
        } else if (board[i] == 9) {
            val[i] =  std::to_string(i);
        }
    }

    //prints out prettified board
    cout << "     |     |     " << endl;
    cout << "  " << val[0] << "  |  " << val[1] << "  |  " << val[2] << endl;
    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;
    cout << "  " << val[3] << "  |  " << val[4] << "  |  " << val[5] << endl;
    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;
    cout << "  " << val[6] << "  |  " << val[7] << "  |  " << val[8] << endl;
    cout << "     |     |     " << endl << endl;
}

//Runs the whole game
void GameBoard::playGame() {

    //output instructions
    cout << "X is player 1 and O is player 2!\n"
            "Computer goes first as X\n"
            "Enter a number on the grid to place your marker there!" << endl;

    //9 moves in total
    for (int i = 0; i < 9; i++) {

        //make the computer move
        cout<<"The board after the computer's move: "<<endl;
        makeComputerMove();

        if (checkWinnerComputer() == 1) {
            cout << "X Won! Impossible!" << endl;
            break;
        } else if (board[0] != 9 && board[1] != 9 && board[2] != 9
                   && board[3] != 9 && board[4] != 9 && board[5] != 9
                   && board[6] != 9 && board[7] != 9 && board[8] != 9) {
            cout << "Draw!" << endl;
            break;
        }

        //make human move by getting input
        getInputOne();
        if (checkWinnerHuman() == 1) {
            cout << "O Won! Pfftt.. Try Harder!" << endl;
            break;
        } else if (board[0] != 9 && board[1] != 9 && board[2] != 9
                   && board[3] != 9 && board[4] != 9 && board[5] != 9
                   && board[6] != 9 && board[7] != 9 && board[8] != 9) {
            cout << "Draw!" << endl;
            break;
        }
    }
}